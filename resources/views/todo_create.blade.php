<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Todo</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">

    </head>
    <body class="antialiased">
        <div class="container">
            <a href="todo_show" class="btn btn-primary">Back</a><br><br>
            <!-- @if($errors->any())
                <div>
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif -->
            <form method="POST" action="todo_submit">
                @csrf
                <table border="1" id="todo_table">
                    <thead>
                        <tr>
                            <td colspan="2" align="center">Add New Record</td>
                        </tr>
                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>
                                <input type="text" name="name"><br>
                                @error('name')
                                    <span style="color: red">{{$message}}</span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="text" name="email">
                                <br>
                                @error('email')
                                    <span style="color: red">{{$message}}</span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="submit" value="Submit"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        
            
    </body>
</html>
