<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Todo</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">

    </head>
    <body class="antialiased">
        <div class="container">
            
            <a href="todo_show" class="btn btn-primary">Back</a><br><br>
            <form method="POST" action="{{route('todo.update',[$todoArr->id])}}">
                @csrf
                <table border="1" id="todo_table">
                    <thead>
                        <tr>
                            <td colspan="2" align="center">Update Record</td>
                        </tr>
                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td><input type="text" name="name" value="{{$todoArr->name}}" required="required"><br>
                                @error('name')
                                    <span style="color: red">{{$message}}</span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="email" name="email" value="{{$todoArr->email}}" required="required">
                                <br>
                                @error('name')
                                    <span style="color: red">{{$message}}</span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="submit" value="Submit"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
            
    </body>
</html>
