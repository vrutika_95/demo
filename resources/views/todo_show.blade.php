<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Todo</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">
        

    </head>
    <body class="antialiased">
        <div class="container">
            <a href="todo_create" class="btn btn-primary" role="button">Add Record</a><br><br>

            {{session('msg')}}<br>
            <table border="1" id="todo_table">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Created Date</td>
                        <td>Action</td>
                    </tr>
                    
                </thead>
                <tbody>
                    @foreach($todoArr as $todo)
                    <tr>
                        <td>{{$todo->id}}</td>
                        <td>{{$todo->name}}</td>
                        <td>{{$todo->email}}</td>
                        <td>{{$todo->created_at}}</td>
                        <td><a href="todo_edit/{{$todo->id}}">Edit</a> | <a href="todo_delete/{{$todo->id}}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </body>
</html>
