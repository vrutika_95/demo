<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('todo_show',[App\Http\Controllers\TodoController::class, 'show']);
Auth::routes();
Route::get('todo_create',[App\Http\Controllers\TodoController::class, 'create']);
Auth::routes();
Route::post('todo_submit',[App\Http\Controllers\TodoController::class, 'store']);
Auth::routes();
Route::get('todo_edit/{id}',[App\Http\Controllers\TodoController::class, 'edit']);
Auth::routes();
Route::post('todo_update/{id}',[App\Http\Controllers\TodoController::class, 'update'])->name('todo.update');
Auth::routes();
Route::get('todo_delete/{id}',[App\Http\Controllers\TodoController::class, 'destroy']);
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
